# menu items:

- PLACES
- KARTS
- PRODUCTS

# endpoints:

[swagger editor](https://www.npmjs.com/package/swagger-editor)

```
/user
  post - sign in

/user/
  get - get current user info

#Place
/place
  get - get all places *
  post - add new place *

/place/{id}
  get - get place with {id} *
  put - update place info *
  delete - delete place with {id} *

/place/geo?longitude=2242&latitude=1212.30 *
    get - find market my geo coords

#Kart
/kart
  get - get all karts *
  post - add new kart *

/kart/{id}
  get - get kart with {id} *
  put - update kart with {id} *
  delete - delete kart with {id} *

/kart/{id}/item
  post - add new item to kart *

/kart/{kart_id}/item/{item_id}
  put - update item info *
  delete - delete item from kart *

#Product
/product
  get - get all products *
  post - add new product *

/product/{id}
  get - get product with {id} *
  put - update product with {id} *
  delete - delete product with {id} *

/product/barcode/{id}
  get - get product by id *

```

# db schema

```sql
drop table if exists item;
drop table if exists kart;
drop table if exists product;
drop table if exists place;
drop table if exists user;

create table user (
  id serial primary key,
  name varchar(50) not null,
  email varchar(50) not null,
  createAt datetime not null,
  modifiedAt: datetime,
  deletedAt: datetime
);

create table place (
  id serial primary key,
  address text,
  name text not null,
  longitude numeric,
  latitude numeric,
  description text,
  user_id integer references user(id) not null,
  createAt datetime not null,
  modifiedAt datetime,
  deletedAt datetime
);

create table kart (
  id serial primary key,
  description text,
  user_id integer references user(id) not null,
  place_id integer references place(id) not null,
  createAt datetime not null,
  modifiedAt datetime,
  deletedAt datetime
);

create table product (
  id serial primary key,
  name text not null,
  callories numeric default 0 not null,
  barcode text,
  pictureUrl text,
  user_id integer references user(id),
  createAt datetime not null,
  modifiedAt datetime,
  deletedAt datetime
);

create table item (
  id serial primary key,
  price numeric(10,2) default 0.00 check(price >= 0.00) not null,
  quantity integer default 1 check(quantity > 0) not null,
  createAt datetime not null,
  kart_id integer references kart(id),
  product_id integer references product(id),
  modifiedAt datetime,
  deletedAt datetime
);
```

# models

User(name:string, email:string) - [id(pk), name, email]

Place ( address:string?, name:string, longitude:number?, latitude:number?, description:string?, user:User) [id(pk), address, name, longitude, latitude, description, user_id(User.id), createdAt, modifiedAt, deletedAt]
^
|1
|
|n
V
Kart (user:User, description:string?, place:Place) [id(pk), date, user_id(User.id), descritption, place_id(Place.id), createdAt, modifiedAt, deletedAt]
^
|1
|
|n
V
Item (price:number, quantity:number) [id(pk), kart_id(Kart.id), product_id(Product.id), price, quantity, createdAt, modifiedAt, deletedAt]
^
|n
|
|1
V
Product (name:string, callories:number?, barcode:string?, pictureUrl:string?, user:User) [id(pk), name, callories, barcode, pictureUrl, user_id(User.id), createdAt, modifiedAt, deletedAt]

# example of kart object

```json
kart {
  id: 123,
  date: '01.01.2019',
  user: {
    id: 123,
    name: 'username',
    email: 'abc@example.org'
  },
  description: 'some description',
  place: {
    id: 234,
    address: 'market address',
    name: 'market name',
    longitude: 123.45,
    latitude: 234.56
  },
  items: [
    {
      id: 123,
      price: 123.45,
      quantity: 1,
      product: {
        id: 12345,
        name: 'product name',
        callories: 12345,
        barcode: '2121233335944334',
        pictureUrl: 'https:/picserver.com/12231231'
      }
    }, {
      id: 124,
      price: 12.30,
      quantity: 2,
      product: {
        id: 12345,
        name: 'another product name',
        callories: 285,
        barcode: '2121233335944334',
        pictureUrl: 'https:/picserver.com/12231231'
      }
    }
  ]
}
```
